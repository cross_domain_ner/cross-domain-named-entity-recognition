# imports
import pandas as pd
import re
from datasets import load_dataset, ClassLabel, Sequence
from collections import Counter
import argparse


def show_all_elements(dataset):
    dfs = []
    for subset_name, subset in dataset.items():
        df = pd.DataFrame(subset)
        df[
            "Subset"
        ] = subset_name  # Add a column to identify the subset (train, test, validation)
        for column, typ in subset.features.items():
            if isinstance(typ, ClassLabel):
                df[column + "_i2l"] = df[column].transform(lambda i: typ.names[i])
            elif isinstance(typ, Sequence) and isinstance(typ.feature, ClassLabel):
                df[column + "_i2l"] = df[column].transform(
                    lambda x: [typ.feature.names[i] for i in x]
                )
        dfs.append(df)

    return pd.concat(
        dfs, ignore_index=True
    )  # Concatenate the DataFrames and return the final result


def count_subset_tags(df):
    counters = {}
    unique_subsets = df["Subset"].unique()

    for subset in unique_subsets:
        subset_tags = df[df["Subset"] == subset]["ner_tags_i2l"]
        flatten_tags = [tag for sublist in subset_tags for tag in sublist]
        tag_frequency = Counter(flatten_tags)
        counters[subset] = tag_frequency

    return counters


def process_entities(dataset_counters):
    # Example frequency data by subset
    data = dataset_counters

    # Convert data to a pandas DataFrame
    df = pd.DataFrame(data)

    # Replace NaN values with zeros
    df = df.fillna(0)
    df = df.sort_values(by="train", ascending=False)

    entities_B = {}
    entities_I = {}

    # Get entities using regex
    for ent in df.index:
        match = re.match(r"(B|I)-([^\d]+)", ent)
        if match:
            if match.group(1) == "B":
                entity_name = match.group(2)
                if entity_name not in entities_B:
                    entities_B[
                        entity_name
                    ] = (
                        {}
                    )  # Initialize the dictionary for the entity if it doesn't exist
                row = df.loc[ent]
                for col in df.columns:
                    entities_B[entity_name][col] = row[col]
            elif match.group(1) == "I":
                entity_name = match.group(2)
                if entity_name not in entities_I:
                    entities_I[
                        entity_name
                    ] = (
                        {}
                    )  # Initialize the dictionary for the entity if it doesn't exist
                row = df.loc[ent]
                for col in df.columns:
                    entities_I[entity_name][col] = row[col]

    data_new = {"B": entities_B, "I": entities_I}

    # Create a DataFrame from the extracted data
    df_result = pd.DataFrame(data_new)

    return df_result


def generate_html_from_dict_of_dataframes(dfs):
    html = ""

    for key, value in dfs.items():
        html += f"<h2>{key}</h2>\n"  # Add title

        html += '<table border="1">\n'  # Start the HTML structure

        # table heading
        html += "<tr>"
        for col in value.columns:
            html += f"<th>{col}</th>"
        html += "</tr>\n"

        # table content
        for row in value.itertuples():
            html += "<tr>"
            for val in row[1:]:
                html += f"<td>{val}</td>"
            html += "</tr>\n"

        html += "</table>\n\n"  # close the table

    return html


if __name__ == "__main__":
    # args parser config
    parser = argparse.ArgumentParser(
        description="Process ner dataset to get the frequency elements."
    )
    parser.add_argument("output_file", help="Html output file name")

    # args parser
    args = parser.parse_args()

    dfs = dict()
    subsets = ["ai", "conll2003", "literature", "music", "politics", "science"]

    entity_subset_mapping = {}  # Mapping of entities to subsets

    for subset in subsets:
        dict_dataset = load_dataset("DFKI-SLT/cross_ner", subset)
        dataset = show_all_elements(dict_dataset)
        dataset_counters = count_subset_tags(dataset)
        df_result = process_entities(dataset_counters)
        df_B = pd.DataFrame(df_result["B"].to_list(), index=df_result.index)
        df_result = pd.concat([df_result.drop(columns="B"), df_B], axis=1)

        df_result = df_result.rename(
            columns={"train": "B_train", "validation": "B_validation", "test": "B_test"}
        )

        df_I = pd.DataFrame(df_result["I"].to_list(), index=df_result.index)
        df_result = pd.concat([df_result.drop(columns="I"), df_I], axis=1)

        df_result = df_result.rename(
            columns={"train": "I_train", "validation": "I_validation", "test": "I_test"}
        )
        df_result['Entity'] = df_result.index

        # Update the entity_subset_mapping
        for entity in df_result['Entity']:
            if entity not in entity_subset_mapping:
                entity_subset_mapping[entity] = set()
            entity_subset_mapping[entity].add(subset)

        dfs[subset] = df_result

    # Find shared and unique entities across subsets
    shared_entities = {entity for entity, subsets in entity_subset_mapping.items() if len(subsets) > 1}
    unique_entities = {entity for entity, subsets in entity_subset_mapping.items() if len(subsets) == 1}

    # Create a dictionary to store the data
    labels_entities_data = {"Shared Entities": {}, "Unique Entities": {}}

    # Store shared entities data
    for entity in shared_entities:
        subsets_list = list(entity_subset_mapping[entity])
        labels_entities_data["Shared Entities"][entity] = subsets_list

    # Store unique entities data
    for entity in unique_entities:
        subset = next(iter(entity_subset_mapping[entity]))
        labels_entities_data["Unique Entities"][entity] = subset

    df_ent = pd.DataFrame.from_dict(labels_entities_data["Shared Entities"], orient='index').transpose()
    df_unq = pd.DataFrame.from_dict(labels_entities_data["Unique Entities"], orient='index').transpose()
    df_unq = df_unq.melt(var_name='Entities', value_name='Subset')
    dfs["Shared Entities"] = df_ent
    dfs["Unique Entities"] = df_unq
    html_output = generate_html_from_dict_of_dataframes(dfs, labels_entities_data)

    with open(args.output_file, "w") as file:
        file.write(html_output)
        print(f"The HTML was saved in {args.output_file}")

