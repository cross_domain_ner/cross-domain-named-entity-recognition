import pytest
from model_experiments import ModelNER
from transformers import Trainer


@pytest.fixture
def model_ner():
    return ModelNER(model_name="bert-base-uncased")


@pytest.fixture
def music_ner_data(model_ner):
    domain = "music"
    dataset_name = "DFKI-SLT/cross_ner"
    data = model_ner.load_dataset(domain, dataset=dataset_name)
    return data


@pytest.fixture
def music_ner_data_tokenized(model_ner, music_ner_data):
    tokenized_datasets = model_ner.map_dataset(music_ner_data)
    return tokenized_datasets


def test_load_dataset(model_ner):
    # Example test for the load_dataset method
    domain = "music"
    dataset_name = "DFKI-SLT/cross_ner"

    data = model_ner.load_dataset(domain, dataset=dataset_name)

    assert "train" in data, "The 'train' key should be present in the loaded dataset"
    assert len(data["train"]["tokens"]) > 0, "The 'tokens' list should not be empty"


def test_map_dataset(model_ner, music_ner_data):
    tokenized_datasets = model_ner.map_dataset(music_ner_data)

    train_data = tokenized_datasets["train"]

    assert (
        "input_ids" in train_data[0]
    ), "The 'input_ids' key should be present in the tokenized dataset"
    assert (
        len(train_data[0]["input_ids"]) > 0
    ), "The 'input_ids' list should not be empty"


def test_training_args(model_ner):
    batch_size = 4
    num_epochs = 1
    learning_rate = 0.001

    training_args = model_ner.training_args(batch_size, num_epochs, learning_rate, "test_trainer")

    assert training_args.per_device_train_batch_size == batch_size
    assert training_args.num_train_epochs == num_epochs
    assert training_args.learning_rate == learning_rate


def test_create_trainer(model_ner, music_ner_data_tokenized):
    # Example test for the create_trainer method
    batch_size = 4
    num_epochs = 1
    learning_rate = 0.001

    training_args = model_ner.training_args(batch_size, num_epochs, learning_rate, "test_trainer")

    trainer = model_ner.create_trainer(
        training_args=training_args,
        train_dataset=music_ner_data_tokenized["train"],
        eval_dataset=music_ner_data_tokenized["test"],
    )
    trainer.train()
    # Example assertions based on your Trainer instance and expected behavior
    assert isinstance(trainer, Trainer)
