# Cross-domain Named Entity Recognition

The goal of the project is to build a system for Named Entity Recognition and to investigate
transfer across domains. We will simulate this setting by using: a dataset in one domain considered
as the source, and datasets in other domains corresponding to our targets. Our goal is to explore
the transfer ability when varying the domains.
The analysis of the results should investigate performance when training and evaluating on the
same domain (baseline system on the source domain), the performance drop when evaluating on
a different domain (transfer learning), the correlation between domain similarity and performance
(source selection / domain adaptation) and the improvement when adding a few target data at
training time (semi-supervised setting).

* Task: named entity recognition
* Setting: domain adaptation
* Datasets:
    -  CrossNER: https://huggingface.co/datasets/DFKI-SLT/cross_ner
    - Source domain: Reuters
    - Target domains: Politics, Natural Science, Music, Literature, and Artificial Intelligence
    - (also a corresponding github: https://github.com/zliucr/CrossNER )
* Authors: Daniela CADENAS LEON, Génesis MONTENEGRO, Martina REPUCCI   
