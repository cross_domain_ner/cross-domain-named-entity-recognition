from datasets import load_dataset
from transformers import (
    AutoModelForTokenClassification,
    AutoTokenizer,
    DataCollatorForTokenClassification,
)
from transformers import TrainingArguments, Trainer
import numpy as np
import evaluate
import torch
import pandas as pd


class ModelNER:
    def __init__(self, model_name, max_len=512):
        self.model_name = model_name
        self.max_len = max_len
        tokenizer_config = {
            "pretrained_model_name_or_path": self.model_name,
            "max_length": self.max_len,
        }
        self.tokenizer = AutoTokenizer.from_pretrained(**tokenizer_config)
        self.dataset = self.load_dataset("music")
        self.label_list, self.label2id, self.id2label = self.id2label_extractor(
            self.dataset["train"]
        )
        self.model = AutoModelForTokenClassification.from_pretrained(
            self.model_name,
            num_labels=len(self.id2label),
            id2label=self.id2label,
            label2id=self.label2id,
            ignore_mismatched_sizes=True,
        )

    def load_dataset(self, domain, dataset="DFKI-SLT/cross_ner"):
        """
        This method uses the datasets.load_dataset() to load an huggingface dataset
        :param domain: one of the subsets available in the dataset
        :param dataset: dataset url, default value == "DFKI-SLT/cross_ner"
        :return: Dataset object
        """
        data = load_dataset(dataset, domain)
        return data

    def id2label_extractor(self, training_dataset):
        label_list = training_dataset.features[f"ner_tags"].feature.names
        id2label = dict()
        label2id = dict()
        for i in range(0, len(label_list)):
            label2id[label_list[i]] = i
            id2label[i] = label_list[i]
        return label_list, label2id, id2label

    def tokenize_and_align_labels(self, examples):
        tokenized_inputs = self.tokenizer(
            examples["tokens"],
            max_length=self.max_len,
            padding="max_length",
            truncation=True,
            is_split_into_words=True,
        )

        labels = []
        for i, label in enumerate(examples[f"ner_tags"]):
            word_ids = tokenized_inputs.word_ids(
                batch_index=i
            )  # Map tokens to their respective word.
            previous_word_idx = None
            label_ids = []
            for word_idx in word_ids:  # Set the special tokens to -100.
                if word_idx is None:
                    label_ids.append(-100)
                elif (
                    word_idx != previous_word_idx
                ):  # Only label the first token of a given word.
                    label_ids.append(label[word_idx])
                else:
                    label_ids.append(-100)
                previous_word_idx = word_idx
            labels.append(label_ids)

        tokenized_inputs["labels"] = labels
        return tokenized_inputs

    def map_dataset(self, dataset):
        """
        Applies the tokenize_dataset method in all the examples, using map function
        :param dataset: objet in dataset
        :return: All dataset tokenized
        """
        return dataset.map(self.tokenize_and_align_labels, batched=True)

    def training_args(self, batch_size, num_epochs, learning_rate, output):
        """
        This method defines the hyperparameters that will be tested in the model
        :param batch_size: number of batches
        :param num_epochs: number of epochs
        :param learning_rate: learning rate value
        :param output: name from the output file
        :return: The training arguments that were defined for the model
        """
        training_args = TrainingArguments(
            output_dir=output,
            per_device_train_batch_size=batch_size,
            num_train_epochs=num_epochs,
            learning_rate=learning_rate,
            evaluation_strategy="epoch",
            do_eval=True,
        )
        return training_args

    def create_trainer(self, training_args, train_dataset, eval_dataset):
        """
        This method executes the training of the model
        :param model: the name of the chosen model
        :param training_args: the hyperparameters previously defined
        :param train_dataset: the name of the training set
        :param eval_dataset: the name of the dev set
        :return: an instance of the Trainer class, trained with the train method
        """
        data_collator = DataCollatorForTokenClassification(tokenizer=self.tokenizer)

        trainer = Trainer(
            model=self.model,
            tokenizer=self.tokenizer,
            args=training_args,
            train_dataset=train_dataset,
            eval_dataset=eval_dataset,
            data_collator=data_collator,
            compute_metrics=self.compute_metrics,
        )
        return trainer

    def predict_labels(self, eval_pred, metric="accuracy"):
        """
        This method presents the predictions and the labels
        :param eval_pred: a tuple containing the logits and the labels
        :param metric: accuracy by default, but can be changed
        :return: accuracy between predictions and labels
        """
        metric = evaluate.load(metric)
        logits, labels = eval_pred
        predictions = np.argmax(logits, axis=-1)
        result = metric.compute(predictions=predictions, references=labels)
        return result

    def compute_metrics(self, p):
        seqeval = evaluate.load("seqeval")
        predictions, labels = p
        predictions = np.argmax(predictions, axis=2)

        true_predictions = [
            [self.label_list[p] for (p, l) in zip(prediction, label) if l != -100]
            for prediction, label in zip(predictions, labels)
        ]
        true_labels = [
            [self.label_list[l] for (p, l) in zip(prediction, label) if l != -100]
            for prediction, label in zip(predictions, labels)
        ]

        results = seqeval.compute(predictions=true_predictions, references=true_labels)
        return {
            "precision": results["overall_precision"],
            "recall": results["overall_recall"],
            "f1": results["overall_f1"],
            "accuracy": results["overall_accuracy"],
        }

    def run_experiment(
        self, domain, batch_size, num_epochs, learning_rate, output_name
    ):
        # dataset creation and tokenization
        self.dataset = self.load_dataset(domain)
        tokenized_datasets = self.map_dataset(self.dataset)

        # training args and trainer definition
        training_args = self.training_args(
            batch_size, num_epochs, learning_rate, output_name
        )
        trainer = self.create_trainer(
            training_args=training_args,
            train_dataset=tokenized_datasets["train"],
            eval_dataset=tokenized_datasets["validation"],
        )
        print("trainer ready to the experiment")
        return trainer

    def ner_inference(self, dev_tokenized_dataset, model):
        """
        Allows to performance a prediction from the test dataset
        :param dev_tokenized_dataset:
        :param model:
        :return:
        """
        preds = []
        sents = []
        subwords = []
        for sent in dev_tokenized_dataset:
            inputs = self.tokenizer(
                sent["tokens"],
                return_tensors="pt",
                padding=True,
                truncation=True,
                is_split_into_words=True,
            )
            inputs = {key: value.to(model.device) for key, value in inputs.items()}
            subtokens = self.tokenizer.convert_ids_to_tokens(
                inputs["input_ids"].squeeze().tolist()
            )
            original_words = [
                word.replace("##", "")
                for word in subtokens
                if not word.startswith("##")
            ]

            with torch.no_grad():
                logits = model(**inputs).logits
            predictions = torch.argmax(logits, dim=2)
            predicted_token_class = [
                model.config.id2label[t.item()] for t in predictions[0]
            ]
            preds.append(predicted_token_class)
            sents.append(original_words)
            subwords.append(subtokens)
        df = pd.DataFrame(
            {"Predictions": preds, "Texts": sents, "Subwords": subwords}
        )
        return df
